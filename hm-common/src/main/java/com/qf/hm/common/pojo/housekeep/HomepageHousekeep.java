package com.qf.hm.common.pojo.housekeep;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * (HomepageHousekeep)实体类
 *
 * @author makejava
 * @since 2023-04-17 14:18:13
 */
@Data
public class HomepageHousekeep implements Serializable {
    private static final long serialVersionUID = 356185133947287388L;
    /**
     * 服务管家id
     */
    private Integer id;
    /**
     * 管家名称
     */
    private String name;
    /**
     * 管家年龄
     */
    private Integer age;
    /**
     * 工作经验
     */
    private Integer seniority;
    /**
     * 服务类型
     */
    private String serviceType;
    /**
     * 籍贯
     */
    private String nativePlace;
    /**
     * 个人简介
     */
    private String evaluate;
    /**
     * 个人介绍
     */
    private String introduce;
    /**
     * 个人展示图片url
     */
    private String avatarUrl;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;


}

