package com.qf.hm.common.constants;

public class CommonConstants {

    public static final int CODE_SUCCESS = 200;
    
    public static final int CODE_ERROR = 500;
}
