package com.qf.hm.common.core;

import com.qf.hm.common.constants.CommonConstants;
import lombok.Data;

@Data
public class AjaxResult {

    private int code;

    private String msg;

    private Object data;

    public static AjaxResult success(Object data) {
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.setCode(CommonConstants.CODE_SUCCESS);
        ajaxResult.setData(data);
        return ajaxResult;
    }

    public static AjaxResult success(int code, Object data) {
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.setCode(code);
        ajaxResult.setData(data);
        return ajaxResult;
    }

    public static AjaxResult error(int code, String msg) {
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.setCode(code);
        ajaxResult.setMsg(msg);
        return ajaxResult;
    }

    public static AjaxResult error(String msg) {
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.setCode(CommonConstants.CODE_ERROR);
        ajaxResult.setMsg(msg);
        return ajaxResult;
    }

}
