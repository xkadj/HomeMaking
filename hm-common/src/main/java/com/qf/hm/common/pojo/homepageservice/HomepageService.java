package com.qf.hm.common.pojo.homepageservice;

import lombok.Data;

import java.util.Date;
import java.io.Serializable;

/**
 * (HomepageService)实体类
 *
 * @author makejava
 * @since 2023-04-18 08:29:57
 */
@Data
public class HomepageService implements Serializable {
    private static final long serialVersionUID = 942059304651302240L;
    /**
     * 首页服务id
     */
    private Integer id;
    /**
     * 服务名称
     */
    private String name;
    /**
     * 服务标题
     */
    private String title;
    /**
     * 价格
     */
    private Integer price;
    /**
     * 服务时长
     */
    private String duration;
    /**
     * 成交量
     */
    private Integer quantity;
    /**
     * 服务内容说明
     */
    private String content;
    /**
     * 费用说明
     */
    private String priceDescription;
    /**
     * 评价id
     */
    private Integer evaluateId;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;


}

