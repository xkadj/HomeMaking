package com.qf.hm.common.utils;

import javax.servlet.http.HttpServletRequest;

public class TokenUtils {

    /**
     * 从请求头获取token
     *
     * @param request
     * @return
     */
    public static String getToken(HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        return token;
    }
}
