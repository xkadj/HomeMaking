package com.qf.hm.common.utils;


import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;
import java.util.Map;

/**
 * jwt工具类
 */
public class JwtUtils {
    // 密钥
    private final static String secret = "123456789qwertyui";

    /**
     * 生成jwt
     *
     * @param claims
     * @return
     */
    public static String createJwt(Map<String, Object> claims) {

        // 签名算法，表示sha256
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        //构造jwt
        JwtBuilder builder = Jwts.builder()//.setHeaderParam("type","jwt") // 设置头
                .setClaims(claims) // 设置载荷
                .setExpiration(new Date(System.currentTimeMillis() + 1800000)) // 设置过期时间
                .signWith(signatureAlgorithm, secret); // 使用指定算法设置签名
        //生成jwt
        return builder.compact();
    }

    /**
     * 解析，如果不符合，报异常
     *
     * @param jsonWebToken
     * @return
     */
    public static Claims parseJWT(String jsonWebToken) {
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(jsonWebToken).getBody();
            return claims;
        } catch (Exception ex) {
            throw new RuntimeException(ex.getMessage());
        }
    }
}
