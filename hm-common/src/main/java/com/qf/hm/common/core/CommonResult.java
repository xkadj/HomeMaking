package com.qf.hm.common.core;

public class CommonResult {

    /**
     * 状态码 1成功 0失败
     */
    private Integer code;
    /**
     * 后台返回给前端的数据
     */
    private Object data;
    /**
     * 后台返回前端的提示信息
     */
    private String msg;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static CommonResult success() {
        CommonResult commonResult = new CommonResult();
        commonResult.setCode(1);
        return commonResult;
    }

    public static CommonResult success(Object data) {
        CommonResult commonResult = new CommonResult();
        commonResult.setCode(1);
        commonResult.setData(data);
        return commonResult;
    }

    public static CommonResult error(String msg) {
        CommonResult commonResult = new CommonResult();
        commonResult.setCode(0);
        commonResult.setMsg(msg);
        return commonResult;
    }
}
