package com.qf.hmapp.models.housekeep.controller;

import com.qf.hm.common.core.CommonResult;
import com.qf.hm.common.pojo.housekeep.HomepageHousekeep;
import com.qf.hmapp.models.housekeep.service.HomepageHousekeepService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * (HomepageHousekeep)表控制层
 *
 * @author makejava
 * @since 2023-04-17 15:17:08
 */
@Api(tags ="管家接口")

@Slf4j
@RestController
@RequestMapping("homepagehousekeep")
public class HomepageHousekeepController {
    /**
     * 服务对象
     */
    @Resource
    private HomepageHousekeepService homepageHousekeepService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("query")
    @ApiOperation(value="查询", notes="根据id查询")
    public CommonResult queryById(@ApiParam("id") Integer id) {

        HomepageHousekeep homepageHousekeep = homepageHousekeepService.queryById(id);
        return CommonResult.success(homepageHousekeep);
    }

    /**
     * 新增数据
     *
     * @param homepageHousekeep 实体
     * @return 新增结果
     */
    @PostMapping
    @ApiOperation(value="添加", notes="添加")
    public ResponseEntity<HomepageHousekeep> add(HomepageHousekeep homepageHousekeep) {
        return ResponseEntity.ok(this.homepageHousekeepService.insert(homepageHousekeep));
    }

    /**
     * 编辑数据
     *
     * @param homepageHousekeep 实体
     * @return 编辑结果
     */
    @PutMapping
    @ApiOperation(value="更新", notes="更新")
    public ResponseEntity<HomepageHousekeep> edit(HomepageHousekeep homepageHousekeep) {
        return ResponseEntity.ok(this.homepageHousekeepService.update(homepageHousekeep));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除是否成功
     */
    @DeleteMapping
    @ApiOperation(value="删除", notes="根据id删除")
    public ResponseEntity<Boolean> deleteById(Integer id) {
        return ResponseEntity.ok(this.homepageHousekeepService.deleteById(id));
    }

}

