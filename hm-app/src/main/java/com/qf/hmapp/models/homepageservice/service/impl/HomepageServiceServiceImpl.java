package com.qf.hmapp.models.homepageservice.service.impl;

import com.qf.hm.common.pojo.homepageservice.HomepageService;
import com.qf.hmapp.models.homepageservice.dao.HomepageServiceDao;
import com.qf.hmapp.models.homepageservice.service.HomepageServiceService;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * (HomepageService)表服务实现类
 *
 * @author makejava
 * @since 2023-04-18 08:37:32
 */
@Service("homepageServiceService")
public class HomepageServiceServiceImpl implements HomepageServiceService {
    @Resource
    private HomepageServiceDao homepageServiceDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public HomepageService queryById(Integer id) {
        return this.homepageServiceDao.queryById(id);
    }

    /**
     * 新增数据
     *
     * @param homepageService 实例对象
     * @return 实例对象
     */
    @Override
    public HomepageService insert(HomepageService homepageService) {
        this.homepageServiceDao.insert(homepageService);
        return homepageService;
    }

    /**
     * 修改数据
     *
     * @param homepageService 实例对象
     * @return 实例对象
     */
    @Override
    public HomepageService update(HomepageService homepageService) {
        this.homepageServiceDao.update(homepageService);
        return this.queryById(homepageService.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.homepageServiceDao.deleteById(id) > 0;
    }
}
