package com.qf.hmapp.models.homepageservice.controller;

import com.qf.hm.common.pojo.homepageservice.HomepageService;
import com.qf.hmapp.models.homepageservice.service.HomepageServiceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * (HomepageService)表控制层
 *
 * @author makejava
 * @since 2023-04-18 08:37:41
 */
@Api(tags ="首页服务接口")

@Slf4j
@RestController
@RequestMapping("homepageService")
public class HomepageServiceController {
    /**
     * 服务对象
     */
    @Resource
    private HomepageServiceService homepageServiceService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("query")
    @ApiOperation(value="查询", notes="根据id查询")
    public ResponseEntity<HomepageService> queryById(Integer id) {
        return ResponseEntity.ok(this.homepageServiceService.queryById(id));
    }

    /**
     * 新增数据
     *
     * @param homepageService 实体
     * @return 新增结果
     */
    @PostMapping
    @ApiOperation(value="添加", notes="添加")
    public ResponseEntity<HomepageService> add(HomepageService homepageService) {
        return ResponseEntity.ok(this.homepageServiceService.insert(homepageService));
    }

    /**
     * 编辑数据
     *
     * @param homepageService 实体
     * @return 编辑结果
     */
    @PutMapping
    @ApiOperation(value="更新", notes="更新")
    public ResponseEntity<HomepageService> edit(HomepageService homepageService) {
        return ResponseEntity.ok(this.homepageServiceService.update(homepageService));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除是否成功
     */
    @DeleteMapping
    @ApiOperation(value="删除", notes="根据id删除")
    public ResponseEntity<Boolean> deleteById(Integer id) {
        return ResponseEntity.ok(this.homepageServiceService.deleteById(id));
    }

}

