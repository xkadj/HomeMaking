package com.qf.hmapp.models.housekeep.dao;


import com.qf.hm.common.pojo.housekeep.HomepageHousekeep;

/**
 * (HomepageHousekeep)表数据库访问层
 *
 * @author makejava
 * @since 2023-04-17 15:13:54
 */
public interface HomepageHousekeepDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    HomepageHousekeep queryById(Integer id);

    /**
     * 统计总行数
     *
     * @param homepageHousekeep 查询条件
     * @return 总行数
     */
    long count(HomepageHousekeep homepageHousekeep);

    /**
     * 新增数据
     *
     * @param homepageHousekeep 实例对象
     * @return 影响行数
     */
    int insert(HomepageHousekeep homepageHousekeep);

    /**
     * 修改数据
     *
     * @param homepageHousekeep 实例对象
     * @return 影响行数
     */
    int update(HomepageHousekeep homepageHousekeep);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

}

