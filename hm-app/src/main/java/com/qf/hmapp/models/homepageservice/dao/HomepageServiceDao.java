package com.qf.hmapp.models.homepageservice.dao;

import com.qf.hm.common.pojo.homepageservice.HomepageService;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * (HomepageService)表数据库访问层
 *
 * @author makejava
 * @since 2023-04-18 08:33:59
 */
public interface HomepageServiceDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    HomepageService queryById(Integer id);

    /**
     * 统计总行数
     *
     * @param homepageService 查询条件
     * @return 总行数
     */
    long count(HomepageService homepageService);

    /**
     * 新增数据
     *
     * @param homepageService 实例对象
     * @return 影响行数
     */
    int insert(HomepageService homepageService);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<HomepageService> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<HomepageService> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<HomepageService> 实例对象列表
     * @return 影响行数
     * @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
     */
    int insertOrUpdateBatch(@Param("entities") List<HomepageService> entities);

    /**
     * 修改数据
     *
     * @param homepageService 实例对象
     * @return 影响行数
     */
    int update(HomepageService homepageService);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

}

