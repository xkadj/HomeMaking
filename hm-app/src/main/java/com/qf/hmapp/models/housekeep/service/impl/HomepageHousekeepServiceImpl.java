package com.qf.hmapp.models.housekeep.service.impl;

import com.qf.hm.common.pojo.housekeep.HomepageHousekeep;
import com.qf.hmapp.models.housekeep.dao.HomepageHousekeepDao;
import com.qf.hmapp.models.housekeep.service.HomepageHousekeepService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * (HomepageHousekeep)表服务实现类
 *
 * @author makejava
 * @since 2023-04-17 15:16:25
 */
@Service
public class HomepageHousekeepServiceImpl implements HomepageHousekeepService {
    @Resource
    private HomepageHousekeepDao homepageHousekeepDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public HomepageHousekeep queryById(Integer id) {
        return this.homepageHousekeepDao.queryById(id);
    }

    /**
     * 新增数据
     *
     * @param homepageHousekeep 实例对象
     * @return 实例对象
     */
    @Override
    public HomepageHousekeep insert(HomepageHousekeep homepageHousekeep) {
        this.homepageHousekeepDao.insert(homepageHousekeep);
        return homepageHousekeep;
    }

    /**
     * 修改数据
     *
     * @param homepageHousekeep 实例对象
     * @return 实例对象
     */
    @Override
    public HomepageHousekeep update(HomepageHousekeep homepageHousekeep) {
        this.homepageHousekeepDao.update(homepageHousekeep);
        return this.queryById(homepageHousekeep.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.homepageHousekeepDao.deleteById(id) > 0;
    }
}
