package com.qf.hmapp.models.housekeep.service;


import com.qf.hm.common.pojo.housekeep.HomepageHousekeep;

/**
 * (HomepageHousekeep)表服务接口
 *
 * @author makejava
 * @since 2023-04-17 15:15:57
 */
public interface HomepageHousekeepService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    HomepageHousekeep queryById(Integer id);

    /**
     * 新增数据
     *
     * @param homepageHousekeep 实例对象
     * @return 实例对象
     */
    HomepageHousekeep insert(HomepageHousekeep homepageHousekeep);

    /**
     * 修改数据
     *
     * @param homepageHousekeep 实例对象
     * @return 实例对象
     */
    HomepageHousekeep update(HomepageHousekeep homepageHousekeep);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

}
