package com.qf.hmapp.models.homepageservice.service;


import com.qf.hm.common.pojo.homepageservice.HomepageService;

/**
 * (HomepageService)表服务接口
 *
 * @author makejava
 * @since 2023-04-18 08:37:21
 */
public interface HomepageServiceService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    HomepageService queryById(Integer id);

    /**
     * 新增数据
     *
     * @param homepageService 实例对象
     * @return 实例对象
     */
    HomepageService insert(HomepageService homepageService);

    /**
     * 修改数据
     *
     * @param homepageService 实例对象
     * @return 实例对象
     */
    HomepageService update(HomepageService homepageService);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

}
