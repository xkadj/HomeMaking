package com.qf.hmapp;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.qf.hmapp.models.*.dao")
public class HmAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(HmAppApplication.class, args);
    }

}
